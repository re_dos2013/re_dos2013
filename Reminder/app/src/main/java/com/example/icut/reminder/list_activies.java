package com.example.icut.reminder;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TabHost;

@SuppressWarnings("deprecation")
public class list_activies extends TabActivity {

    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_activies);

        TabHost tabHost = getTabHost();



        TabHost.TabSpec seminar = tabHost.newTabSpec("Seminar");
        seminar.setIndicator("Seminar", getResources().getDrawable(R.drawable.style_seminar));
        Intent seminarIntent = new Intent(this, seminar.class);
        seminar.setContent(seminarIntent);



        TabHost.TabSpec rapat = tabHost.newTabSpec("Meeting");
        rapat.setIndicator("Meeting", getResources().getDrawable(R.drawable.style_rapat));
        Intent rapatIntent = new Intent(this, rapat.class);
        rapat.setContent(rapatIntent);



        TabHost.TabSpec ajar = tabHost.newTabSpec("Teaching");
        ajar.setIndicator("Teaching", getResources().getDrawable(R.drawable.style_ajar));
        Intent ajarIntent = new Intent(this, ajar.class);
        ajar.setContent(ajarIntent);


        TabHost.TabSpec keg_lain = tabHost.newTabSpec("Others Activity");
        keg_lain.setIndicator("Others Activity", getResources().getDrawable(R.drawable.style_kegiatan_lain));
        Intent keg_lainIntent = new Intent(this, ajar.class);
        keg_lain.setContent(keg_lainIntent);


        tabHost.addTab(seminar);
        tabHost.addTab(rapat);
        tabHost.addTab(ajar);
        tabHost.addTab(keg_lain);
    }

}
