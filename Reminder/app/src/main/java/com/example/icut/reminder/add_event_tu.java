package com.example.icut.reminder;

import java.util.List;
import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class add_event_tu extends Activity {
    Spinner kategori;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event_tu);
        kategori = (Spinner) findViewById(R.id.spinner);

        //untuk membuat list kategori, atau bisa menggunaan String[]
        List<String> item = new ArrayList<String>();
        item.add("Seminar");
        item.add("Meeting");
        item.add("Teaching");
        item.add("Others Activity");

        //untuk membuat adapter list kategori
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (add_event_tu.this,android.R.layout.simple_spinner_dropdown_item,item);

        //untuk menentukan model adapter nya
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //menerapkan adapter pada spinner sp
        kategori.setAdapter(adapter);


        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //fab.setOnClickListener(new View.OnClickListener() {
        //  @Override
        //public void onClick(View view) {
        //  Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
        //        .setAction("Action", null).show();

    }

}
